<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\PokemonsController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [AuthController::class, 'register']);
Route::post('login', [AuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function(){
    Route::get('logout', [AuthController::class, 'logout']);
    Route::post('search', [AuthController::class, 'search']);
    Route::post('edit-profile', [AuthController::class, 'editProfile']);
    Route::get('user', [AuthController::class, 'user']);
    Route::post('pokemon/like', [PokemonsController::class, 'like']);
    Route::post('pokemon/dislike', [PokemonsController::class, 'dislike']);


});