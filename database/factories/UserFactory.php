<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        
            
            

        $gender = rand(1,2);
        $name = ($gender==2)? $this->faker->name('female') : $this->faker->name('male');
        $arr_name = explode(" ",$name); 
    

        $background_colors = ['D32F2F', 'C2185B', '7B1FA2','512DA8','303F9F','1976D2','00796B','388E3C','388E3C','388E3C','E64A19','455A64'];
        $color_count = count($background_colors)-1;
        $default_image = "https://ui-avatars.com/api/?name=".$arr_name[0]."+".$arr_name[1]."&size=128&background=".$background_colors[rand(0, $color_count)]."&color=fff&format=svg";
        
        return [
            
            'image_url' => $default_image,
            'first_name' => $arr_name['0'] ,
            'last_name' => $arr_name['1'],
            'birthdate' =>  rand(1970,2003).'-'.rand(1,12).'-'.rand(1,28),
            'email' => fake()->unique()->safeEmail(),
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return static
     */
    public function unverified()
    {
        return $this->state(fn (array $attributes) => [
            'email_verified_at' => null,
        ]);
    }
}
